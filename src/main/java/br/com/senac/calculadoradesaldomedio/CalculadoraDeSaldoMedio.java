package br.com.senac.calculadoradesaldomedio;

public class CalculadoraDeSaldoMedio {

    public double calcularSaldoMedido(double salario) {

        if (salario >= 0 && salario <= 500) {
            return 0;
        } else if (salario < 1001) {
            return salario * 0.3;
        } else if (salario < 3001) {
            return salario * 0.4;
        }

        return salario * 0.5;
    }

}
